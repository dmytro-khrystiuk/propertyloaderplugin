package org.bitbucket.theromefather.plugins.mojos;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.springframework.core.io.FileSystemResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

import java.io.IOException;
import java.util.List;

@Mojo(name = "loadProperties",
      defaultPhase = LifecyclePhase.INITIALIZE,
      threadSafe = true)
public class LoadProperties extends AbstractMojo {

    @Parameter(property = "silent",
               defaultValue = "false")
    private boolean silent;

    @Parameter(property = "locations")
    private List<String> locations;

    @Parameter(defaultValue = "${project}",
               readonly = true,
               required = true)
    private MavenProject project;

    private final ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(new FileSystemResourceLoader());

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        getLog().debug("Project object before: " + project.getProperties().toString());
        getLog().info("Received " + locations.size() + " locations : " + locations);
        for (String location : locations) {
            getLog().info("Processing location '" + location + "'");
            resolvePropertiesForLocation(location);
        }
        getLog().debug("Project object after: " + project.getProperties().toString());
    }

    private void resolvePropertiesForLocation(String path) {
        try {
            for (Resource resource : resolver.getResources("/" + path.trim())) {
				getLog().debug("Processing resource '" + resource.getFilename() + "'");
                if (resource.getFile().isFile()) {
                    getLog().debug("Processing file '" + resource.getFilename() + "'");
                    project.getProperties().load(resource.getInputStream());
                }
            }
        } catch (IOException e) {
            if (!silent) {
                throw new IllegalArgumentException(e);
            }
        }
    }

}
